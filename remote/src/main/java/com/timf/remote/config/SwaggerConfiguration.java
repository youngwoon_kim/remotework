package com.timf.remote.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2 //swagger를 활성화
public class SwaggerConfiguration {

    @Bean
    public Docket swagger(){ //swagger api설정
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(swaggerInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.timf.remote.controller")).paths(PathSelectors.any())
                .paths(PathSelectors.any())
                .build().useDefaultResponseMessages(false);
    }

    private ApiInfo swaggerInfo(){ //문서설명과 작성자 노출 가능
        return new ApiInfoBuilder().title("Spring API Documentation").description("테스트")
                .license("youngwoon.kim")
                .licenseUrl("http://remote.timf.co.kr").version("1").build();
    }

}
