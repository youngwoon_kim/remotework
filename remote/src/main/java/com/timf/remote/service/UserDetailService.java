package com.timf.remote.service;

import com.timf.remote.advice.exception.CustomMemberNotFoundException;
import com.timf.remote.repoistory.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserDetailService implements UserDetailsService {
    private final MemberRepository memberRepository;

    @Override
    public UserDetails loadUserByUsername(String memId){
        return memberRepository.findById(Long.valueOf(memId)).orElseThrow(CustomMemberNotFoundException::new);
    }
}
