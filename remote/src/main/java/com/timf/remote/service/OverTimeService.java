package com.timf.remote.service;

import com.timf.remote.dto.request.MemberJoinDto;
import com.timf.remote.dto.request.OverTimeModifyDto;
import com.timf.remote.dto.request.OverTimeSuggestDto;
import com.timf.remote.dto.response.MemberInfoDto;
import com.timf.remote.dto.response.OverTimeInfoDto;
import com.timf.remote.model.*;
import com.timf.remote.repoistory.AttendanceRepository;
import com.timf.remote.repoistory.MemberRepository;
import com.timf.remote.repoistory.OverTimeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Transactional(readOnly = true)
@RequiredArgsConstructor
@Service
public class OverTimeService {

    private final OverTimeRepository overTimeRepository;
    private final MemberRepository memberRepository;
    private final AttendanceRepository attendanceRepository;

    // 연장 근무 요청
    @Transactional
    public Long suggest(OverTimeSuggestDto overTimeSuggestDto) {

        Attendance attendance = attendanceRepository.findById(overTimeSuggestDto.getAttendanceId()).get();
        Member member = memberRepository.findById(overTimeSuggestDto.getMemberId()).get();

        OverTime buildOverTime = OverTime.builder()
                .suggestDate(LocalDate.now())
                .startDate(overTimeSuggestDto.getStartDate())
                .endDate(overTimeSuggestDto.getEndDate())
                .status(OtStatus.fromString(overTimeSuggestDto.getStatus()))
                .content(overTimeSuggestDto.getContent())
                .member(member)
                .attendance(attendance)
                .build();

        return overTimeRepository.save(buildOverTime).getOtId();
    }

    // 연장 근무 전체 조회
    public Page<OverTimeInfoDto> findAll(Pageable page) {
        Page<OverTime> findAll = overTimeRepository.findAll(page);
        return findAll.map(OverTimeInfoDto::new);
    }

    // 승인자 기준 조회
    public Page<OverTimeInfoDto> findAllByApproverId(Long id, Pageable page) {
        Page<OverTime> findAll = overTimeRepository.findAllByMemberMemId(id, page);
        return findAll.map(OverTimeInfoDto::new);
    }

    // 본인 기준 조회
    public Page<OverTimeInfoDto> findAllByUserId(Long id, Pageable page) {
        Page<OverTime> findAll = overTimeRepository.findAllByAttendance_MemberMemId(id,page);
        return findAll.map(OverTimeInfoDto::new);
    }

    // 신청 승인
    @Transactional
    public void statusApproved(Long id) {
        OverTime overTime = overTimeRepository.findById(id).get();
        overTime.statusApproved();
        overTimeRepository.save(overTime);
    }

    // 신청 반려
    @Transactional
    public void statusRejected(Long id) {
        OverTime overTime = overTimeRepository.findById(id).get();
        overTime.statusRejected();
        overTimeRepository.save(overTime);
    }

    // 연장 근무 삭제
    @Transactional
    public void delete(Long id) {
        overTimeRepository.deleteById(id);
    }

    // 연장 근무 수정
    @Transactional
    public Long modify(OverTimeModifyDto overTimeModifyDto) {
        OverTime overTime = overTimeRepository.findById(overTimeModifyDto.getOtId()).get();

        if (overTime.getStatus() == OtStatus.WAITING) {
            OverTime buildOverTime = OverTime.builder()
                    .content(overTimeModifyDto.getContent())
                    .startDate(overTimeModifyDto.getStartDate())
                    .endDate(overTimeModifyDto.getEndDate())
                    .suggestDate(overTime.getSuggestDate())
                    .status(overTime.getStatus())
                    .member(overTime.getMember())
                    .attendance(overTime.getAttendance())
                    .build();

            overTimeRepository.deleteById(overTime.getOtId());
            return overTimeRepository.save(buildOverTime).getOtId();
        }
        return 0L;
    }


}
