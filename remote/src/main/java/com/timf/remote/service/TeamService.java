package com.timf.remote.service;

import com.timf.remote.dto.TeamDto;
import com.timf.remote.model.Team;
import com.timf.remote.repoistory.TeamRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TeamService {
    private final TeamRepository teamRepository;

    // 팀 자동 등록
    @Transactional
    public void saveTeams() {
        String[] teamList = {"SA", "SD1", "SD2", "MA", "IT", "SL", "PL"};

        for (String s : teamList) {
            Team team = Team.builder()
                    .name(s)
                    .build();
            teamRepository.save(team);
        }
    }
    // 팀 등록
    @Transactional
    public Long save(TeamDto teamDto) {
        validateDuplicateTeam(teamDto);
        Team team = Team.builder()
                .name(teamDto.getName())
                .build();
        teamRepository.save(team);
        return team.getTeamId();
    }

    // 중복 검사
    private void validateDuplicateTeam(TeamDto teamDto) {
        List<Team> findTeams = teamRepository.findAllByName(teamDto.getName());
        if(!findTeams.isEmpty()) {
            throw new IllegalStateException("이미 존재하는 팀 명입니다.");
        }

    }




}
