package com.timf.remote.service;

import com.timf.remote.dto.request.AttendanceEditDto;
import com.timf.remote.dto.request.AttendanceSaveDto;
import com.timf.remote.dto.response.AttendanceBaseDto;
import com.timf.remote.model.AttStatus;
import com.timf.remote.model.Attendance;
import com.timf.remote.model.Member;
import com.timf.remote.model.Space;
import com.timf.remote.repoistory.AttendanceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Transactional(readOnly = true)
@Service
public class AttendanceService {

    private final MemberService memberService;
    private final AttendanceRepository attendanceRepository;

    @Transactional
    public Long save(AttendanceSaveDto attendanceSaveDto){
        //퇴근시간이 이미 포함되어있는 경우 예외처리 안되어있음. (>퇴근시간이 갱신된다...)
        //전날 퇴근 정보가 없는데 오늘 출근버튼을 누른경우 예외처리 필요
        return attendanceRepository.save(todayWork(attendanceSaveDto)).getAttId();
    }

    //오늘 등록된 근태내역이 있으면 퇴근처리, 없으면 새로생성
    private Attendance todayWork(AttendanceSaveDto attendanceSaveDto){

        Attendance findAttendance = attendanceRepository.findByMemberMemIdAndDate(attendanceSaveDto.getMemberId(), attendanceSaveDto.getDate());

        if(findAttendance == null){
            Member findMember = memberService.findOneMember(attendanceSaveDto.getMemberId());

            return Attendance.builder()
                    .date(attendanceSaveDto.getDate())
                    .status(AttStatus.fromString(attendanceSaveDto.getAttStatus()))
                    .onTime(LocalDateTime.now())
                    .space(Space.fromString(attendanceSaveDto.getSpace()))
                    .member(findMember)
                    .build();
        }

        findAttendance.addOffTime(LocalDateTime.now());

        return findAttendance;
    }

    //특정 멤버의 전체 근태내역을 출력
    public Page<AttendanceBaseDto> findAllByMemberId(Long id, Pageable pageable){
        Page<Attendance> result = attendanceRepository.findAllByMemberMemId(id, pageable);
        return result.map(AttendanceBaseDto::new);
    }

    //전체멤버의 전체 근태내역을 출력
    public Page<AttendanceBaseDto> findAll(Pageable pageable){
        Page<Attendance> findAll = attendanceRepository.findAll(pageable);
        return findAll.map(AttendanceBaseDto::new);
    }

    //특정 기간 사이의 근태내역을 출력
    public Page<AttendanceBaseDto> findByDays(LocalDate startDay, LocalDate endDay, Pageable pageable){
        Page<Attendance> result = attendanceRepository.findByDateBetween(startDay, endDay, pageable);
        return result.map(AttendanceBaseDto::new);
    }

    //특정 기간 사이의 특정 멤버의 근태내역을 출력
    public Page<AttendanceBaseDto> findByDaysWithMember(Long memberId, LocalDate startDay, LocalDate endDay, Pageable pageable){
        Page<Attendance> result = attendanceRepository.findByMemberMemIdAndDateBetween(memberId, startDay, endDay, pageable);
        return result.map(AttendanceBaseDto::new);
    }

    //근태내역이 없는경우 새로 등록, 근태내역이 있는 경우 기존 근태를 수정 > 지난날 등록에도 사용
    @Transactional
    public Long editAttendance(AttendanceEditDto attendanceEditDto) {
        Attendance findOne = attendanceRepository.findByMemberMemIdAndDate(attendanceEditDto.getMemberId(), attendanceEditDto.getDate());

        if(findOne == null)
        {
            Member findMember = memberService.findOneMember(attendanceEditDto.getMemberId());
            Attendance build = Attendance.builder()
                    .date(attendanceEditDto.getDate())
                    .onTime(attendanceEditDto.getOnTime())
                    .status(AttStatus.AFTER)
                    .member(findMember)
                    .build();
            build.addOffTime(attendanceEditDto.getOffTime());
            return attendanceRepository.save(build).getAttId();
        }
        findOne.editAttendance(findOne.getOnTime(),attendanceEditDto.getOffTime());
        return attendanceRepository.save(findOne).getAttId();
    }
}
