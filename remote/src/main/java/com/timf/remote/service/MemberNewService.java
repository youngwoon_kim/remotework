package com.timf.remote.service;

import com.timf.remote.advice.exception.CustomEmailSignInFailedException;
import com.timf.remote.advice.exception.CustomMemberNotFoundException;
import com.timf.remote.dto.request.MemberEditPwdDto;
import com.timf.remote.dto.request.NewMemberJoinDto;
import com.timf.remote.dto.request.NewMemberLoginDto;
import com.timf.remote.model.Member;
import com.timf.remote.model.Team;
import com.timf.remote.repoistory.MemberRepository;
import com.timf.remote.repoistory.TeamRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberNewService {
    private final MemberRepository memberRepository;
    private final TeamRepository teamRepository;
    private final PasswordEncoder passwordEncoder;

    // 회원 가입
    @Transactional
    public String join(NewMemberJoinDto newMemberJoinDto){
        Team team = teamRepository.findById(newMemberJoinDto.getTeamId()).get();

        Member member = Member.builder()
                .name(newMemberJoinDto.getName())
                .email(newMemberJoinDto.getEmail())
                .password(passwordEncoder.encode(newMemberJoinDto.getPassword()))
                .authority(newMemberJoinDto.getAuthority())
                .joinDate(newMemberJoinDto.getJoinDate())
                .team(team)
                .build();
        memberRepository.save(member);
        return member.getEmail();
    }

    // 회원 단건조회
    @Transactional(readOnly = true)
    public List findAll() {
        return memberRepository.findAll();
    }

    //로그인을 위한 이메일 조회
    @Transactional(readOnly = true)
    public Member signIn(NewMemberLoginDto newMemberLoginDto){
        Member member = memberRepository.findByEmail(newMemberLoginDto.getEmail()).orElseThrow(CustomMemberNotFoundException::new);

        if(!passwordEncoder.matches(newMemberLoginDto.getPassword(), member.getPassword()))
            throw new CustomEmailSignInFailedException();

        return member;
    }

    @Transactional
    public void edit(){

    };


    @Transactional
    public void editPwd(MemberEditPwdDto memberEditPwdDto) {

        Member member = memberRepository.findByEmail(memberEditPwdDto.getEmail()).get();
        String pwd = passwordEncoder.encode(memberEditPwdDto.getPassword());
        member.editPwd(pwd);

    }


}
