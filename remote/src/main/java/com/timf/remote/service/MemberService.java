package com.timf.remote.service;

import com.timf.remote.dto.request.MemberJoinDto;
import com.timf.remote.dto.response.MemberInfoDto;
import com.timf.remote.model.Member;
import com.timf.remote.repoistory.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Transactional(readOnly = true)
@RequiredArgsConstructor
@Service
public class MemberService {

    private final MemberRepository memberRepository;

    @Transactional
    public Long Join(MemberJoinDto memberJoinDto){
        //중복가입 예외처리
        return memberRepository.save(memberJoinDto.toEntity()).getMemId();
    }

    public List<MemberInfoDto> findAll(){
        List<Member> findAll = memberRepository.findAll();
        return findAll.stream().map(MemberInfoDto::new).collect(Collectors.toList());
    }

    public MemberInfoDto findOne(Long id){
        return new MemberInfoDto(memberRepository.findById(id).get());
    }

    protected Member findOneMember(Long id){
        return memberRepository.findById(id).get();
    }


}
