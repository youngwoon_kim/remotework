package com.timf.remote.advice.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class CustomEmailSignInFailedException extends RuntimeException{

    public CustomEmailSignInFailedException(String msg) {
        super(msg);
    }

    public CustomEmailSignInFailedException(String msg, Throwable t) {
        super(msg, t);
    }
}
