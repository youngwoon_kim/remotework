package com.timf.remote.repoistory;

import com.timf.remote.model.Attendance;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface AttendanceRepository extends JpaRepository<Attendance,Long> {

    @EntityGraph(attributePaths = {"member"})
    Attendance findByMemberMemIdAndDate(Long memberId, LocalDate localdate);

    @EntityGraph(attributePaths = {"member"})
    Page<Attendance> findAllByMemberMemId(Long memberId, Pageable pageable);

    @EntityGraph(attributePaths = {"member"})
    Page<Attendance> findByDateBetween(LocalDate startDate, LocalDate endDate, Pageable pageable);

    @EntityGraph(attributePaths = {"member"})
    Page<Attendance> findByMemberMemIdAndDateBetween(Long memberId, LocalDate startDate, LocalDate endDate, Pageable pageable);

    @Override
    @EntityGraph(attributePaths = {"member"})
    Page<Attendance> findAll(Pageable pageable);
}
