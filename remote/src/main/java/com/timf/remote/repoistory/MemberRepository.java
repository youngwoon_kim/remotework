package com.timf.remote.repoistory;

import com.timf.remote.model.Member;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MemberRepository extends JpaRepository<Member, Long> {

//    List<Member> findByAuthority(Authority authority, Authority authority2);
    Optional<Member> findByEmail(String email);
}
