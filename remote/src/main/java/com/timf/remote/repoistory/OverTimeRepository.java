package com.timf.remote.repoistory;

import com.timf.remote.model.Attendance;
import com.timf.remote.model.Member;
import com.timf.remote.model.OverTime;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;

public interface OverTimeRepository extends JpaRepository<OverTime, Long> {

    Page<OverTime> findAllByMemberMemId(Long memberId, Pageable page);
    Page<OverTime> findAllByAttendance_MemberMemId(Long memberId, Pageable page);


}
