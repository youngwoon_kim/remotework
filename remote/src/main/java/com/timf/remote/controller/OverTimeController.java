package com.timf.remote.controller;

import com.timf.remote.config.jwt.JwtTokenProvider;
import com.timf.remote.dto.request.OverTimeModifyDto;
import com.timf.remote.dto.request.OverTimeSuggestDto;
import com.timf.remote.dto.response.OverTimeInfoDto;
import com.timf.remote.model.OverTime;
import com.timf.remote.repoistory.OverTimeRepository;
import com.timf.remote.service.OverTimeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.Response;
import java.util.List;

@Api(tags = { "4. OverTime"})
@RequiredArgsConstructor
@RestController
public class OverTimeController {

    private final OverTimeService overTimeService;
    private final JwtTokenProvider jwtTokenProvider;

    // 연장 근무 신청
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-AUTH-TOKEN", value = "로그인 성공 후 access_token", required = true, dataType = "String", dataTypeClass = String.class, paramType = "header") })
    @ApiOperation(value = "연장근무 등록", notes = "등록 테스트")
    @PostMapping("/overtime")
    public Long suggestOverTime(@RequestBody OverTimeSuggestDto overTimeSuggestDto) {
        return overTimeService.suggest(overTimeSuggestDto);
    }

    // 연장 근무 조회(전체)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-AUTH-TOKEN", value = "로그인 성공 후 access_token", required = true, dataType = "String", dataTypeClass = String.class, paramType = "header") })
    @ApiOperation(value = "모든 회원 연장근무 조회", notes = "조회 테스트")
    @GetMapping("/overtime")
    public Page<OverTimeInfoDto> searchAll(Pageable pageable) {
        return overTimeService.findAll(pageable);
    }

    // 연장 근무 조회(한사람)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-AUTH-TOKEN", value = "로그인 성공 후 access_token", required = true, dataType = "String", dataTypeClass = String.class, paramType = "header") })
    @ApiOperation(value = "한사람 연장근무 조회(승인자 기준)", notes = "조회 테스트")
    @GetMapping("/overtime/approver/{id}")
    public Page<OverTimeInfoDto> searchOneByApprover(@PathVariable Long id, Pageable pageable) {
        return overTimeService.findAllByApproverId(id, pageable);
    }

    // 연장 근무 조회(한사람)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-AUTH-TOKEN", value = "로그인 성공 후 access_token", required = true, dataType = "String", dataTypeClass = String.class, paramType = "header") })
    @ApiOperation(value = "한사람 연장근무 조회(사용자 기준)", notes = "조회 테스트")
    @GetMapping("/overtime/{id}")
    public Page<OverTimeInfoDto> searchOneByUser(@PathVariable Long id, Pageable pageable) {

        return overTimeService.findAllByUserId(id, pageable);
    }

    // 연장 근무 반려
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-AUTH-TOKEN", value = "로그인 성공 후 access_token", required = true, dataType = "String", dataTypeClass = String.class, paramType = "header") })
    @ApiOperation(value = "연장 근무 반려", notes = "상태 변경 테스트")
    @PutMapping("/overtime/rejected/{id}")
    public void rejected(@PathVariable Long id) {
        overTimeService.statusRejected(id);
    }

    // 연장 근무 반려
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-AUTH-TOKEN", value = "로그인 성공 후 access_token", required = true, dataType = "String", dataTypeClass = String.class, paramType = "header") })
    @ApiOperation(value = "연장 근무 승인", notes = "상태 변경 테스트")
    @PutMapping("/overtime/approved/{id}")
    public void approved(@PathVariable Long id) {
        overTimeService.statusApproved(id);
    }

    // 연장 근무 삭제
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-AUTH-TOKEN", value = "로그인 성공 후 access_token", required = true, dataType = "String", dataTypeClass = String.class, paramType = "header") })
    @ApiOperation(value = "연장 근무 삭제", notes = "삭제 테스트")
    @DeleteMapping("/overtime/{id}")
    public void delete(@PathVariable Long id) {
        overTimeService.delete(id);
    }

    // 연장 근무 수정
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-AUTH-TOKEN", value = "로그인 성공 후 access_token", required = true, dataType = "String", dataTypeClass = String.class, paramType = "header") })
    @ApiOperation(value = "연장 근무 수정", notes = "수정 테스트")
    @PutMapping("/overtime")
    public String modify(@RequestBody OverTimeModifyDto overTimeModifyDto) {
        Long isOk = overTimeService.modify(overTimeModifyDto);
        if(isOk == 0L) {
            return "fail";
        }
        return "ok";
    }



}
