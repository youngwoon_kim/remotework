package com.timf.remote.controller;

import com.timf.remote.dto.TeamDto;
import com.timf.remote.service.TeamService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = { "1.Team " })
@RequiredArgsConstructor
@RestController
public class TeamController {

    private final TeamService teamService;

    @ApiOperation(value = "팀 자동 등록", notes = "기존 팀 자동 등록을 한다.")
    @PostMapping("/teams")
    public void saveTeams() {
        teamService.saveTeams();
    }

    @ApiOperation(value = "팀 등록", notes = "신규 팀 등록을 한다.")
    @PostMapping("/team")
    public Long save(@RequestBody TeamDto teamDto) {
        return teamService.save(teamDto);
    }


}
