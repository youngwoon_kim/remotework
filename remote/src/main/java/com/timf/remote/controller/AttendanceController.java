package com.timf.remote.controller;

import com.timf.remote.config.jwt.JwtTokenProvider;
import com.timf.remote.dto.request.AttendanceEditDto;
import com.timf.remote.dto.request.AttendanceSaveDto;
import com.timf.remote.dto.response.AttendanceBaseDto;
import com.timf.remote.model.Member;
import com.timf.remote.service.AttendanceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@Api(tags = {"3. Attendance"})
@RequiredArgsConstructor
@RestController
public class AttendanceController {

    private final AttendanceService attendanceService;
    private final JwtTokenProvider jwtTokenProvider;


    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-AUTH-TOKEN", value = "로그인 성공 후 access_token", required = true, dataType = "String", dataTypeClass = String.class, paramType = "header") })
    @ApiOperation(value = "근태등록", notes = "근태내용을 등록한다.")
    @PostMapping("/attendance")
    public long addWork(@RequestBody AttendanceSaveDto attendanceSaveDto, HttpServletRequest request){

        String header = request.getHeader("X-AUTH-TOKEN");
        System.out.println("header = " + header);

        String memId = jwtTokenProvider.getMemId(header);
        String email = jwtTokenProvider.getEmail(header);

        System.out.println("memId = " + memId);
        System.out.println("email = " + email);

        return attendanceService.save(attendanceSaveDto);
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-AUTH-TOKEN", value = "로그인 성공 후 access_token", required = true, dataType = "String", dataTypeClass = String.class, paramType = "header")})
    @ApiOperation(value = "근태조회", notes = "특정멤버의 전체 근태내역을 조회한다")
    @GetMapping("/attendance/{memberId}")
    public Page<AttendanceBaseDto> searchOne(@PathVariable("memberId") Long id, Pageable pageable){
        return attendanceService.findAllByMemberId(id, pageable);
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-AUTH-TOKEN", value = "로그인 성공 후 access_token", required = true, dataType = "String", dataTypeClass = String.class, paramType = "header") })
    @ApiOperation(value = "근태조회", notes = "모든 근태내역을 조회한다")
    @GetMapping("/attendance")
    public Page<AttendanceBaseDto> searchAll(Pageable pageable){
        return attendanceService.findAll(pageable);
    }

    //수정필요
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-AUTH-TOKEN", value = "로그인 성공 후 access_token", required = true, dataType = "String", dataTypeClass = String.class, paramType = "header") })
    @ApiOperation(value ="근태조회", notes = "기간별 근태내역을 조회한다")
    @GetMapping("/attendance/days")
    public Page<AttendanceBaseDto> searchDays(@RequestParam("startDay") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate startDay,
                                              @RequestParam("endDay") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endDay,
                                              Pageable pageable){
        return attendanceService.findByDays(startDay,endDay, pageable);
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-AUTH-TOKEN", value = "로그인 성공 후 access_token", required = true, dataType = "String", dataTypeClass = String.class, paramType = "header") })
    @ApiOperation(value ="근태조회", notes = "기간별 회원 근태내역을 조회한다")
    @GetMapping("/attendance/days/{memberId}")
    public Page<AttendanceBaseDto> searchDaysByMember(@PathVariable("memberId") Long memberId,
                                                      @RequestParam("startDay") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate startDay,
                                                      @RequestParam("endDay") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endDay,
                                                      Pageable pageable) {
        return attendanceService.findByDaysWithMember(memberId,startDay, endDay, pageable);
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-AUTH-TOKEN", value = "로그인 성공 후 access_token", required = true, dataType = "String", paramType = "header") })
    @ApiOperation(value = "근태수정", notes ="근태 내역을 수정한다")
    @PutMapping("/attendance")
    public Long editWork(@Valid @RequestBody AttendanceEditDto attendanceEditDto){
        return attendanceService.editAttendance(attendanceEditDto);
    }

}
