package com.timf.remote.controller;

import com.timf.remote.config.jwt.JwtTokenProvider;
import com.timf.remote.dto.request.MemberEditPwdDto;
import com.timf.remote.dto.request.NewMemberJoinDto;
import com.timf.remote.dto.request.NewMemberLoginDto;
import com.timf.remote.model.Member;
import com.timf.remote.model.response.CommonResult;
import com.timf.remote.model.response.SingleResult;
import com.timf.remote.service.MemberNewService;
import com.timf.remote.service.ResponseService;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CachePut;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Api(tags = { "3.MemberNew" })
@RequiredArgsConstructor
@RestController
public class MemberController {
    private final MemberNewService memberNewService;
    private  final JwtTokenProvider jwtTokenProvider;
    private final ResponseService responseService;
    private final PasswordEncoder passwordEncoder;

    @ApiOperation(value = "가입", notes = "회원가입을 한다.")
    @PostMapping(value = "/signup")
    public CommonResult signUp(@ApiParam(value = "MemberNewJoinDto 객체", required = true) @RequestBody NewMemberJoinDto newMemberJoinDto) {
        return responseService.getSingleResult(memberNewService.join(newMemberJoinDto));
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-AUTH-TOKEN", value = "로그인 성공 후 access_token", required = true, dataType = "String", dataTypeClass = String.class, paramType = "header") })
    @ApiOperation(value = "관리자 회원등록", notes = "관리자가 회원을 등록한다.")
    @PostMapping(value = "/admin/signup")
    public CommonResult signUpAdmin(@ApiParam(value = "MemberNewJoinDto 객체", required = true)
                                        @RequestBody NewMemberJoinDto newMemberJoinDto, HttpServletRequest request) {



        return responseService.getSingleResult(memberNewService.join(newMemberJoinDto));
    }

    @ApiOperation(value = "로그인", notes = "이메일 회원 로그인을 한다.")
    @PostMapping(value = "/signin")
    public SingleResult<String> signIn(@ApiParam(value = "NewMemberLoginDto", readOnly = true) @RequestBody NewMemberLoginDto newMemberLoginDto) {
        Member member = memberNewService.signIn(newMemberLoginDto);
        String token = jwtTokenProvider.createToken(String.valueOf(member.getMemId()), member.getEmail(), member.getAuthorityList());

        return responseService.getSingleResult(token);
    }

    @GetMapping(value = "/test")
    public void test(@RequestBody @Valid NewMemberJoinDto newMemberJoinDto, @AuthenticationPrincipal NewMemberLoginDto memberLoginDto) {
        System.out.println(memberLoginDto + "memberLoginDto");
    }

    @ApiOperation(value = "패스워드 변경", notes = "패스워드를 변경 한다.")
    @PutMapping(value = "/editPwd")
    public void editPwd(@RequestBody MemberEditPwdDto memberEditPwdDto) {
        memberNewService.editPwd(memberEditPwdDto);
    }
}
