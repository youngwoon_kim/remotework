package com.timf.remote.model;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Entity
public class OverTime extends BaseEntity {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ot_id")
    private Long otId;

    @Column(name = "ot_suggest_date", nullable = false)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate suggestDate;

    @Column(name="ot_start_dt", nullable = false)
    private LocalDateTime startDate;

    @Column(name="ot_end_dt",nullable = false)
    private LocalDateTime endDate;

    @Column(name = "ot_ct")
    private String content;

    @Enumerated(EnumType.STRING)
    @Column(name = "ot_st", nullable = false, length = 20)
    private OtStatus status;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="att_id")
    private Attendance attendance;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="mem_id")
    private Member member;

    @Builder
    public OverTime(LocalDate suggestDate, LocalDateTime startDate, LocalDateTime endDate, String content, OtStatus status,Member member, Attendance attendance) {
        this.suggestDate = suggestDate;
        this.startDate = startDate;
        this.endDate = endDate;
        this.content = content;
        this.status = status;
        this.attendance = attendance;
        this.member = member;
    }

    public void statusApproved() {
        this.status = OtStatus.APPROVED;
    }

    public void statusRejected() {
        this.status = OtStatus.REJECTED;
    }


}
