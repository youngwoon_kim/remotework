package com.timf.remote.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.timf.remote.dto.request.MemberEditPwdDto;
import lombok.*;
import org.hibernate.annotations.DynamicInsert;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@Getter
@Entity
@Builder
public class Member extends BaseEntity implements UserDetails{

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "mem_id")
    private Long memId;

    @Column(name = "mem_name", nullable = false, length = 16)
    private String name;

    @Column(name = "mem_email", nullable = false, length = 35, unique = true)
    private String email;

    @Column(name = "mem_password", nullable = false, length = 200)
    private String password;

    @Column(name = "mem_tel_num", length = 16)
    private String telNum;

    @Column(name = "mem_address", length = 200)
    private String address;

    @Column(name = "mem_position", length = 100)
    private String position;

    @Column(name = "mem_role", length = 50)
    private String role;

    @Column(name = "mem_join_date", nullable = false)
    private LocalDate joinDate;

    @Column(name = "mem_leave_date")
    private LocalDate leaveDate;

    @OneToMany(mappedBy = "member")
    private List<Attendance> attendanceList = new ArrayList<>();

    @OneToMany(mappedBy = "member")
    private List<OverTime> overTimeList = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "team_id")
    private Team team;


    // 새로운 방식
    @Column(name = "mem_authority")
    private String authority;

    public List<String> getAuthorityList() {
        if(this.authority.length() > 0) {
            return Arrays.asList(this.authority.split(","));
        }
        return new ArrayList<>();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        getAuthorityList().forEach(r -> {
            authorities.add(()->r);
        });
        return authorities;
    }
    /*  이전 사용하던 방식
    @Enumerated(EnumType.STRING)
    @Column(name = "mem_authority", nullable = false, length = 20)
    private Authority authority;


    @ElementCollection(fetch = FetchType.EAGER)
    @Builder.Default
    private List<String> roles = new ArrayList<>();

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
    }*/


    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Override
    public String getUsername() {
        return this.email;
    }
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Override
    public boolean isEnabled() {
        return true;
    }

    public void editPwd(String pwd) {
        this.password = pwd;
    }
}
