package com.timf.remote.model;

public enum DeleteStatus {
    Y, N
}
