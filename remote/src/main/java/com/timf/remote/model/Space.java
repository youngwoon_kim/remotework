package com.timf.remote.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum Space {
    INSIDE_WORK("내근"),
    OUTSIDE_WORK("외근");

    @Getter
    private String name;

    Space(String name) { this.name = name; }

    public static Space fromString(String name){
        for( Space s : Space.values()){
            if(s.name.equalsIgnoreCase(name))
                return s;
        }
        return Space.INSIDE_WORK;
    }

}
