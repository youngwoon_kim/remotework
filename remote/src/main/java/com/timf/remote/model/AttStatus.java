package com.timf.remote.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum AttStatus {
    BEFORE("출근전"),
    WORKING("근무중"),
    AFTER("퇴근"),
    VACATION("휴가");

    @Getter
    private String name;

    AttStatus(String name){
        this.name = name;
    }

    public static AttStatus fromString(String name){
        for (AttStatus a : AttStatus.values()) {
            if (a.name.equalsIgnoreCase(name))
                return a;
        }
        return AttStatus.BEFORE;
    }

}
