package com.timf.remote.model;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Entity
public class Attendance extends BaseEntity{

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "att_id")
    private Long attId;

    @Column(name = "att_date", nullable = false)
    private LocalDate date;

    @Column(name ="att_on_dt")
    private LocalDateTime onTime;

    @Column(name ="att_off_dt")
    private LocalDateTime offTime;

    @Enumerated(EnumType.STRING)
    @Column(name ="att_st", nullable = false, length = 20)
    private AttStatus status;

    @Enumerated(EnumType.STRING)
    @Column(name ="att_space", length = 20)
    private Space space;


    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "mem_id")
    private Member member;

    @OneToMany(mappedBy = "attendance")
    private List<OverTime> overTimeList = new ArrayList<>();

    @Builder
    public Attendance(LocalDate date, LocalDateTime onTime, LocalDateTime offTime, AttStatus status, Space space, Member member){
        this.date = date;
        this.onTime = onTime;
        this.offTime = offTime;
        this.status = status;
        this.space = space;
        this.member = member;
    }

    public void editAttendance(LocalDateTime onTime, LocalDateTime offTime){
        this.onTime = onTime;
        addOffTime(offTime);
    }

    public void addOffTime(LocalDateTime offTime){
        this.offTime = offTime;
        status = AttStatus.AFTER;
    }

}
