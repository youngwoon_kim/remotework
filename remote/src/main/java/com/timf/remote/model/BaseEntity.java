package com.timf.remote.model;


import lombok.Getter;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.sql.Delete;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@EntityListeners(AuditingEntityListener.class)
@MappedSuperclass
public class BaseEntity {

    @CreatedDate
    @Column(updatable = false)
    private LocalDateTime createdDate;

    @LastModifiedDate
    private LocalDateTime modifiedDate;

    @CreatedBy
    @Column(updatable = false)
    private String createBy;

    @LastModifiedBy
    private String modifiedBy;

    @Enumerated(EnumType.STRING)
    @Column(name = "delete_st" ,nullable = false)
    private DeleteStatus deleteStatus;

    @PrePersist
    public void preSetting() {
        deleteStatus = DeleteStatus.N;
    }

}
