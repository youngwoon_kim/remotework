package com.timf.remote.model;

import lombok.Getter;

public enum OtStatus {
    REJECTED("거절"),
    APPROVED("승인"),
    WAITING("대기");

    @Getter
    private String name;

    OtStatus(String name) {
        this.name = name;
    }

    public static OtStatus fromString(String name){
        for (OtStatus a : OtStatus.values()) {
            if (a.name.equalsIgnoreCase(name))
                return a;
        }
        return OtStatus.WAITING;
    }
}
