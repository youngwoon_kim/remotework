package com.timf.remote.dto.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.timf.remote.model.AttStatus;
import com.timf.remote.model.Attendance;
import com.timf.remote.model.Space;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@NoArgsConstructor
@Getter
public class AttendanceSaveDto {

    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate date;

    private String attStatus;

    @NotNull
    private Long memberId;

    private String space;


}
