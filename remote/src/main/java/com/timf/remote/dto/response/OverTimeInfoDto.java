package com.timf.remote.dto.response;

import com.timf.remote.model.OtStatus;
import com.timf.remote.model.OverTime;
import lombok.Getter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
public class OverTimeInfoDto {
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private String content;
    private String status;
    private LocalDate suggestDate;
    private Long memberId;
    private Long attendanceId;
    private Long suggestId;

    public OverTimeInfoDto(OverTime overTime) {
        suggestId = overTime.getAttendance().getMember().getMemId(); // 연장 근무 신청자
        startDate = overTime.getStartDate();
        endDate = overTime.getEndDate();
        content = overTime.getContent();
        status = overTime.getStatus().getName();
        suggestDate = overTime.getSuggestDate();
        memberId = overTime.getMember().getMemId(); // 연장 근무 승인자
        attendanceId = overTime.getAttendance().getAttId();
    }
}
