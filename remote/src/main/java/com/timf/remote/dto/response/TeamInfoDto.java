package com.timf.remote.dto.response;

import com.timf.remote.model.Team;
import lombok.Getter;

@Getter
public class TeamInfoDto {

    private String name;

    public TeamInfoDto(Team team){
        name = team.getName();
    }

}
