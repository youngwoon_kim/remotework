package com.timf.remote.dto.response;

import com.timf.remote.model.Member;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
public class MemberInfoDto {
    @NotNull
    private Long memId;

    private String name;
    @NotNull
    private String email;

    private String telNum;

    private String address;

    private String authority;

    private LocalDate joinDate;

    private LocalDate leaveDate;



    public MemberInfoDto(Member member){
        memId = member.getMemId();
        name = member.getName();
        email = member.getEmail();
        telNum = member.getTelNum();
        address = member.getAddress();
        authority = member.getAuthority();
        joinDate = member.getJoinDate();
        leaveDate = member.getLeaveDate();

    }

}
