package com.timf.remote.dto.request;

import com.timf.remote.model.OtStatus;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor
public class OverTimeSuggestDto {

    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private String content;
    private String status;
    private LocalDate suggestDate;
    private Long memberId; // 승인자 정보
    private Long attendanceId;

}
