package com.timf.remote.dto.request;

import com.timf.remote.model.Member;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor
public class MemberJoinDto {

    private String name;

    private String email;

    private String password;

    private String authority;

    private LocalDate joinDate;

    public void encodePassword(String password){
        this.password = password;
    }

    public Member toEntity(){

        return Member.builder()
                .name(name)
                .email(email)
                .password(password)
                .authority(authority)
                .joinDate(joinDate)
                .build();
    }

}
