package com.timf.remote.dto.request;

import lombok.Getter;

import java.time.LocalDate;

@Getter
public class AttendanceSearchDto {

    private LocalDate startDate;

    private LocalDate endDate;

    private Long memberId;

}
