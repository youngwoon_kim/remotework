package com.timf.remote.dto.request;

import lombok.Getter;

@Getter
public class NewMemberLoginDto {
    private String email;
    private String password;
}
