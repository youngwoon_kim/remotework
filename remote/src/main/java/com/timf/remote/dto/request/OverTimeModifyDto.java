package com.timf.remote.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor
public class OverTimeModifyDto {

    private Long otId;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private String content;

}
