package com.timf.remote.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class MemberEditPwdDto {

    private String email;
    private String id;
    private String password;
    private String name;

}
