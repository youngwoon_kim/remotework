package com.timf.remote.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.timf.remote.model.AttStatus;
import com.timf.remote.model.Attendance;
import lombok.Getter;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Optional;

@Getter
public class AttendanceBaseDto {

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate date;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private LocalDateTime onTime;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private LocalDateTime offTime;

    @DateTimeFormat(pattern = "HH:mm:ss")
    private LocalTime dayWorkTime;

    private String status;

    private String memberName;

    public AttendanceBaseDto(Attendance attendance){
        date = attendance.getDate();
        onTime = attendance.getOnTime();
        offTime = attendance.getOffTime();
        status = attendance.getStatus().getName();
        memberName = Optional
               .ofNullable(attendance.getMember().getName())
                .orElse("사용자 정보 없음");
    }


}
